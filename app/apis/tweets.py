from flask_restplus import Namespace, Resource, fields
from app.core.tweets import (
    get_tweets,
    get_tweet_by_id,
    create_tweet,
    update_tweet,
    delete_tweet,
)
from app.core.exceptions import (
    NotFoundException,
)


api = Namespace("tweets")


tweet = api.model("Tweet", {
    "id": fields.Integer,
    "text": fields.String(required=True),
})


post_tweet = api.model("Post tweet", {
    "text": fields.String(required=True),
})


errors = api.model("Errors", {
    "status": fields.String(required=True),
    "description": fields.String(required=True),
})


@api.route('')
class Tweets(Resource):
    @api.doc("get_tweets")
    @api.response(code=200, description="Success", model=[tweet])
    def get(self):
        return get_tweets(), 200

    @api.doc("create_tweet")
    @api.expect(post_tweet)
    @api.response(code=201, description="Success", model=tweet)
    @api.response(code=409, description="Please enter a message", model=errors)
    def post(self):
        if len(api.payload["text"]) > 0:
            new_tweet = create_tweet(api.payload["text"])
            return new_tweet, 201
        else:
            return "Please enter a message", 409


@api.route('/<int:id>')
class Tweet(Resource):
    @api.doc("get_tweet_by_id")
    @api.response(code=200, description="Success", model=tweet)
    @api.response(code=404, description="No tweet found for this id", model=errors)
    def get(self, id):
        try:
            return get_tweet_by_id(id), 200
        except NotFoundException as e:
            return e.as_dict(), e.status_code

    @api.doc("update_tweet")
    @api.expect(post_tweet)
    @api.response(code=200, description="Success", model=tweet)
    @api.response(code=404, description="No tweet found for this id", model=errors)
    def put(self, id):
        try:
            if len(api.payload["text"]) > 0:
                updated_tweet = update_tweet(id, api.payload["text"])
                return updated_tweet, 200
            else:
                return "Please enter a message", 409
        except NotFoundException as e:
            return e.as_dict(), e.status_code

    @api.doc("delete_tweet")
    @api.response(code=204, description="Deleted")
    @api.response(code=404, description="No tweet found for this id", model=errors)
    def delete(self, id):
        try:
            delete_tweet(id)
            return None, 204
        except NotFoundException as e:
            return e.as_dict(), e.status_code
